package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.Calendar;
import at.mavila.scheduler.jpa.entities.Email;
import at.mavila.scheduler.jpa.entities.User;
import at.mavila.scheduler.jpa.entities.UserDetails;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.commons.lang3.RandomUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by mavila on 09.09.16.
 */
@ContextConfiguration(locations = {"/META-INF/spring-config-in-memory.xml"})
@EnableJpaRepositories
@EnableTransactionManagement
@Transactional
public class CalendarRepositoryInMemoryTest extends UserRepositoryInMemoryTest {


    private static final Logger LOGGER = LoggerFactory.getLogger(CalendarRepositoryInMemoryTest.class);
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected EmailRepository emailRepository;
    @Autowired
    protected RoleRepository roleRepository;

    @Autowired
    protected ActionRepository actionRepository;

    @Autowired
    protected CalendarRepository calendarRepository;


    @Override
    @BeforeClass
    public void setUp() throws Exception{
        super.setUp();
    }


    @Test
    public void testCalendarOwner(){

        User pedrito = new User();
        pedrito.setRole(this.roleRepository.findByRoleName("scheduler"));
        Email email = new Email();
        email.setUser(pedrito);
        email.setValue(
                RandomStringUtils.randomAlphabetic(3)+
                "@"+
                RandomStringUtils.randomAlphabetic(5)+
                "."+
                RandomStringUtils.randomAlphabetic(3));
        email.setPrimary(true);
        pedrito.addEmail(email);

        UserDetails pedritoDetails = new UserDetails();
        pedritoDetails.setFirstName("Pedro");
        pedritoDetails.setLastName("Vargas");
        pedritoDetails.setUsername("padrote");
        pedritoDetails.setPassword(RandomStringUtils.random(10));
        pedritoDetails.setSalt(RandomStringUtils.random(10));
        pedrito.setUserDetails(pedritoDetails);

        this.userRepository.save(pedrito);
        this.userRepository.flush();


        //In another place

        Calendar calendar = new Calendar();
        calendar.setOwner(pedrito);
        calendar.setName("pedrito_calendar");
        //calendar.addSubscriber(pedrito);
        this.calendarRepository.save(calendar);
        this.calendarRepository.flush();


        calendar.addSubscriber(pedrito);

        this.calendarRepository.save(calendar);
        this.calendarRepository.flush();



    }

}