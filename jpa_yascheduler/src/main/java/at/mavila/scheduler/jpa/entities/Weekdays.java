package at.mavila.scheduler.jpa.entities;

/**
 * Created by mavila on 10.09.16.
 */
public enum Weekdays {
    SUNDAY,MONDAY,TUESDAY,WEDNESDAY,THURSDAY,FRIDAY,SATURDAY;
}
