package at.mavila.scheduler.jpa.entities;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import javax.persistence.Column;
import javax.persistence.Embeddable;
import java.io.Serializable;

/**
 * Entity implementation class for Entity: UserDetails
 */
@Embeddable
@AutoProperty
public class UserDetails implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -4371875973465661724L;


    @Column(name = "username", nullable = false, length = 50, unique = true)
    private String username;

    @Column(name = "password", nullable = false, length = 255)
    private String password;

    @Column(name = "salt", nullable = false, length = 255)
    private String salt;


    @Column(name = "firstName", nullable = true, length = 50, unique = false)
    private String firstName;
    @Column(name = "lastName", nullable = true, length = 50, unique = false)
    private String lastName;

    public UserDetails() {
        super();
    }

    public String getFirstName() {
        return this.firstName;
    }

    public void setFirstName(final String firstName1) {
        this.firstName = firstName1;
    }

    public String getLastName() {
        return this.lastName;
    }

    public void setLastName(final String lastName1) {
        this.lastName = lastName1;
    }


    public String getUsername() {
        return this.username;
    }

    public void setUsername(final String username1) {
        this.username = username1;
    }

    public String getPassword() {
        return this.password;
    }

    public void setPassword(final String password1) {
        this.password = password1;
    }

    public String getSalt() {
        return this.salt;
    }

    public void setSalt(String salt1) {
        this.salt = salt1;
    }

    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }

}
