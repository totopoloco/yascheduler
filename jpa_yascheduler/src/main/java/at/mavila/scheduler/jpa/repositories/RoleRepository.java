package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.Role;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

/**
 * Created by mavila on 27.08.16.
 */
public interface RoleRepository extends JpaRepository<Role, Long> {
	
    @Query(" from Role r where r.name=:name")
    Role findByRoleName(@Param("name") final String name);
    
}
