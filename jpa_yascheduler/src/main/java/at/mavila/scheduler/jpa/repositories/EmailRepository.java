package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.Email;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by mavila on 27.08.16.
 */
public interface EmailRepository extends JpaRepository<Email, Long> {

    @Query(" from Email u where u.user.userDetails.username=:username order by u.isPrimary desc")
    List<Email> findByUsername(@Param("username") final String username);

    @Query(" from Email u where u.user.userID=:userID order by u.isPrimary desc")
    List<Email> findByUserID(@Param("userID") final Long userID);

}
