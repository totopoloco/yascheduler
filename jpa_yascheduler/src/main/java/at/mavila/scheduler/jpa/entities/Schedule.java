package at.mavila.scheduler.jpa.entities;

import at.mavila.scheduler.jpa.util.LoggerEntityListener;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.*;
import java.io.Serializable;
import java.util.Date;

/**
 * Created by mavila on 10.09.16.
 */

@Entity
@Table(name = "CALENDAR_ENTRY")
@AutoProperty
@EntityListeners(value = {LoggerEntityListener.class})
public class Schedule implements Serializable {

    @Id
    @SequenceGenerator(name = "schedule_id_seq", sequenceName = "schedule_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "schedule_id_seq")
    private Long scheduleID;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_START", nullable = false)
    private java.util.Date dateStart;

    @Temporal(TemporalType.DATE)
    @Column(name = "DATE_END", nullable = true)
    private java.util.Date dateEnd;

    @Column(name = "DESCRIPTION", nullable = true)
    private String description;

    @Column(name = "ENABLED", nullable = false)
    private boolean isEnabled;

    @Enumerated(EnumType.ORDINAL)
    private Weekdays weekdays;


    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "calendarID", nullable = false)
    @Property(policy = PojomaticPolicy.NONE)
    private Calendar calendar;

    public Long getScheduleID() {
        return scheduleID;
    }

    public void setScheduleID(Long calendarEntryID) {
        this.scheduleID = calendarEntryID;
    }

    public Date getDateStart() {
        return dateStart;
    }

    public void setDateStart(Date dateStart) {
        this.dateStart = dateStart;
    }

    public Date getDateEnd() {
        return dateEnd;
    }

    public void setDateEnd(Date dateEnd) {
        this.dateEnd = dateEnd;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public boolean isEnabled() {
        return isEnabled;
    }

    public void setEnabled(boolean enabled) {
        isEnabled = enabled;
    }

    public Weekdays getWeekdays() {
        return weekdays;
    }

    public void setWeekdays(Weekdays weekdays) {
        this.weekdays = weekdays;
    }

    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }


}
