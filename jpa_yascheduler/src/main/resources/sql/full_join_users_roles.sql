SELECT
  u.username AS username,
  r.name     AS role_name
FROM USERS u
  INNER JOIN user_role ur ON ur.userID = u.userID
  INNER JOIN ROLES r ON ur.roleID = r.roleID
UNION ALL
SELECT
  t1.username,
  t1.role_name
FROM
  (SELECT
     u.username AS username,
     r.name     AS role_name
   FROM USERS u
     LEFT JOIN user_role ur ON ur.userID = u.userID
     LEFT JOIN ROLES r ON ur.roleID = r.roleID
   ORDER BY u.username, r.name) t1
WHERE t1.role_name IS NULL
UNION ALL
SELECT
  t1.username,
  t1.role_name
FROM
  (SELECT
     u.username AS username,
     r.name     AS role_name
   FROM USERS u
     RIGHT JOIN user_role ur ON ur.userID = u.userID
     RIGHT JOIN ROLES r ON ur.roleID = r.roleID
   ORDER BY u.username, r.name) t1
WHERE t1.username IS NULL;
