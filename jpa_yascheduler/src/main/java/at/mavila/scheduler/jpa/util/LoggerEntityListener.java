package at.mavila.scheduler.jpa.util;



import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.persistence.*;

public class LoggerEntityListener {

    private static final Logger LOGGER = LoggerFactory.getLogger(LoggerEntityListener.class);

    public LoggerEntityListener(){
        //Does nothing.
    }
    @PrePersist
    void onPrePersist(Object o) {
        final String message = String.format("onPrePersist: %s ",o==null?"":o.toString());
        LOGGER.info(message);

    }
    @PostPersist
    void onPostPersist(Object o) {

        final String message = String.format("onPostPersist: %s ",o==null?"":o.toString());
        LOGGER.info(message);


    }
    @PostLoad
    void onPostLoad(Object o) {

        final String message = String.format("onPostLoad: %s ",o==null?"":o.toString());
        LOGGER.info(message);

    }
    @PreUpdate
    void onPreUpdate(Object o) {
        final String message = String.format("onPreUpdate: %s ",o==null?"":o.toString());
        LOGGER.info(message);

    }
    @PostUpdate void onPostUpdate(Object o) {
        final String message = String.format("onPostUpdate: %s ",o==null?"":o.toString());
        LOGGER.info(message);

    }
    @PreRemove void onPreRemove(Object o) {
        final String message = String.format("onPreRemove: %s ",o==null?"":o.toString());
        LOGGER.info(message);

    }
    @PostRemove void onPostRemove(Object o) {
        final String message = String.format("onPostRemove: %s ",o==null?"":o.toString());
        LOGGER.info(message);

    }
}
