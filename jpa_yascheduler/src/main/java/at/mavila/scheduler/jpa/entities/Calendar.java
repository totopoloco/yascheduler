package at.mavila.scheduler.jpa.entities;

import at.mavila.scheduler.jpa.util.LoggerEntityListener;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mavila on 09.09.16.
 */
@Entity
@Table(name = "CALENDAR")
@AutoProperty
@EntityListeners(value = {LoggerEntityListener.class})
public class Calendar implements Serializable{

    @Id
    @SequenceGenerator(name = "calendar_id_seq", sequenceName = "calendar_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "calendar_id_seq")
    private Long calendarID;


    @Column(name = "name", nullable = false, length = 50, unique = false)
    private String name;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userID", nullable = false)
    private User owner;

    @Property(policy= PojomaticPolicy.NONE)
    @ManyToMany(cascade = CascadeType.ALL)
    @JoinTable(name = "CALENDAR_SUBSCRIBED",
            joinColumns = { @JoinColumn(name = "calendarID") },
            inverseJoinColumns = { @JoinColumn(name = "userID") })
    private List<User> subscribers = new ArrayList<>();


    @OneToMany(mappedBy = "calendar", fetch = FetchType.LAZY)
    @Property(policy = PojomaticPolicy.NONE)
    private Set<Schedule> schedules = new HashSet<>();


    /*Getters and setters*/

    public String getName() {
        return name;
    }


    public void setName(String name) {
        this.name = name;
    }

    public User getOwner() {
        return owner;
    }

    public void setOwner(User owner) {
        this.owner = owner;
    }

    public List<User> getSubscribers() {
        return subscribers;
    }

    public void setSubscribers(List<User> subscribers) {
        this.subscribers = subscribers;
    }

    public void addSubscriber(User subscriber){
        if(subscriber==null){
            return;
        }
        this.subscribers.add(subscriber);
    }


    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }

}
