package at.mavila.scheduler.jpa.specifications;

import at.mavila.scheduler.jpa.entities.User;
import org.apache.commons.lang3.Validate;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.springframework.data.jpa.domain.Specification;

import javax.persistence.criteria.*;
import java.util.List;


/**
 * Created by mavila on 27.08.16.
 */
@AutoProperty
public class UserUsernameSpecification implements Specification<User> {

    private final String filter;

    public UserUsernameSpecification(String filter) {
        super();
        Validate.notNull(filter, "User filter can't be null");
        this.filter = filter;
    }

    /**
     * select
     * user0_.userID as userID1_0_,
     * user0_.email as email2_0_,
     * user0_.password as password3_0_,
     * user0_.salt as salt4_0_,
     * user0_.username as username5_0_
     * from
     * USERS user0_
     * where
     * user0_.username like ?
     *
     * @param root
     * @param query
     * @param cb
     * @return
     */
    @Override
	public Predicate toPredicate(Root<User> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
        final Predicate predicate = cb.conjunction();
        final List<Expression<Boolean>> expressions = predicate.getExpressions();
        expressions.add(cb.like(root.get("userDetails").<String>get("username"), this.filter));
        return predicate;
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }
}
