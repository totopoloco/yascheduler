package at.mavila.scheduler.jpa.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by mavila on 09.09.16.
 */
@ContextConfiguration(locations = {"/META-INF/spring-config-mysql.xml"})
@EnableJpaRepositories
@EnableTransactionManagement
@Transactional
public class CalendarRepositoryMySQLTest extends CalendarRepositoryInMemoryTest {
    private static final Logger LOGGER = LoggerFactory.getLogger(CalendarRepositoryMySQLTest.class);

    @Override
    @BeforeClass(enabled = false)
    public void setUp() throws Exception{
        super.setUp();
    }


    @Test(enabled = false)
    @Override
    public void testCalendarOwner(){

        super.testCalendarOwner();



    }

}