package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.Calendar;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mavila on 09.09.16.
 */
public interface CalendarRepository extends JpaRepository<Calendar, Long>{
}
