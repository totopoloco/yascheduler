package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.Schedule;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mavila on 10.09.16.
 */
public interface ScheduleRepository extends JpaRepository<Schedule,Long> {
}
