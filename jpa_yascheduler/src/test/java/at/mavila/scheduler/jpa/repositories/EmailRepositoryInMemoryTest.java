package at.mavila.scheduler.jpa.repositories;

import static org.assertj.core.api.Assertions.assertThat;

import java.util.List;

import org.apache.log4j.BasicConfigurator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import at.mavila.scheduler.jpa.entities.Email;

/**
 * Created by mavila on 27.08.16.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring-config-in-memory.xml"})
@EnableJpaRepositories
@EnableTransactionManagement
public class EmailRepositoryInMemoryTest extends AbstractTestNGSpringContextTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(EmailRepositoryInMemoryTest.class);

    @Autowired
    protected EmailRepository emailRepository;


    @BeforeClass
    public static void setUp() throws Exception {
        BasicConfigurator.configure();
    }

    @Test
    public void testFindByUsername() throws Exception {

        final List<Email> emailsFromUser = emailRepository.findByUsername("andybuston");

        LOGGER.info("Emails from user {}",emailsFromUser);
        assertThat(emailsFromUser).isNotNull();

    }

}