SELECT roleID,FK_CHILD_ROLE,name,description
FROM ROLES p
WHERE NOT EXISTS
(
   SELECT 1 FROM ROLES c
   WHERE c.roleID = p.FK_CHILD_ROLE
   AND c.name <> c.name	
) order by name