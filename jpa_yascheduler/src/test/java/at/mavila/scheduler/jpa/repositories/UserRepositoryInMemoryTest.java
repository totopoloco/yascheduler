package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.*;
import at.mavila.scheduler.jpa.specifications.UserUsernameSpecification;
import org.apache.commons.lang3.RandomStringUtils;
import org.apache.log4j.BasicConfigurator;
import org.junit.Assert;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.testng.AbstractTestNGSpringContextTests;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

/**
 * Created by mavila on 27.08.16.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring-config-in-memory.xml"})
@EnableJpaRepositories
@EnableTransactionManagement
@Transactional
public class UserRepositoryInMemoryTest extends AbstractTestNGSpringContextTests {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryInMemoryTest.class);
    @Autowired
    protected UserRepository userRepository;
    @Autowired
    protected EmailRepository emailRepository;
    @Autowired
    protected RoleRepository roleRepository;

    @Autowired
    protected ActionRepository actionRepository;

    @BeforeClass
    public void setUp() throws Exception {

        //Role

        BasicConfigurator.configure();

        Role admin = new Role("admin", "administrator of the system, can do all");

        Role scheduler = new Role("scheduler", "can create/delete schedules");
        Role service = new Role("service", "Only stops/start the site");
        Role user = new Role("user", "can apply for appointments in the schedules");

        //Scheduler
        scheduler.setParentRole(admin);
        admin.addChildRole(scheduler);
        
        
        user.setParentRole(admin);
        
        service.setParentRole(admin);
        admin.addChildRole(service);

        //Actions
        /*Login*/
        Action login = new Action();
        login.setName("login");

        login.addRole(admin);
        login.addRole(scheduler);
        login.addRole(user);
        login.addRole(service);

        this.actionRepository.save(login);
        /*End login*/

        /*Register on behalf other users*/
        Action register = new Action();
        register.setName("register");
        this.actionRepository.save(register);
        register.addRole(admin);
        admin.addAction(register);
        this.actionRepository.save(register);
        /*End*/


        /*Register other users*/
        Action sregister = new Action();
        sregister.setName("self_register");
        this.actionRepository.save(sregister);
        sregister.addRole(user);
        user.addAction(sregister);
        this.actionRepository.save(sregister);
        /*End*/

    }

    @Test
    public void testInsertUserAdminWithTwoEmails() {
        //Insertion

        try {

            //Given
            final User user = new User();
            
            UserDetails userDetails = new UserDetails();
            userDetails.setPassword("passwordPlainForNow");
            userDetails.setSalt("saltPlainForNow");
            userDetails.setUsername("juanga");
            userDetails.setFirstName(RandomStringUtils.randomAlphabetic(5));
            userDetails.setLastName(RandomStringUtils.randomAlphabetic(5));
            
            user.setUserDetails(userDetails);

            Email email1 = new Email();
            email1.setUser(user);
            email1.setValue("hola@gmail.com");
            email1.setPrimary(true);

            Email email2 = new Email();
            email2.setUser(user);
            email2.setValue("que_onda@gmail.com");
            email2.setPrimary(false);

            user.addEmail(email1);
            user.addEmail(email2);


            final Role roleAdmin = this.roleRepository.findByRoleName("admin");
            //user.addRole(roleAdmin);
            user.setRole(roleAdmin);
            roleAdmin.addUser(user);


            //When
            final User savedUser = this.userRepository.save(user);
            //Then
            assertThat(savedUser).isNotNull();


            //---------------------------------------------------------------

            //When
            final Iterable<Email> emailIterable = this.emailRepository.save(savedUser.getEmails());

            //Then
            assertThat(savedUser.getEmails()).hasSize(2);//It must have two emails.
            assertThat(savedUser.getEmails()).containsAll(emailIterable);

            retrieval();

        } catch (Exception e) {
            LOGGER.error("Exception caught {}", e.getMessage());
            Assert.fail();
        }
    }

    @Test
    public void testInsertUserWithTwoEmails() {
        try {
            //Insertion
            final User user2 = new User();
            final UserDetails userDetails = new UserDetails();
            
            userDetails.setPassword("passwordPlainForNow2");
            userDetails.setSalt("saltPlainForNow2");
            userDetails.setUsername("evitamunoz");
            userDetails.setFirstName(RandomStringUtils.randomAlphabetic(5));
            userDetails.setLastName(RandomStringUtils.randomAlphabetic(5));
            
            user2.setUserDetails(userDetails);

            Email email3 = new Email();
            email3.setUser(user2);
            email3.setValue("any@mailinatore.com");
            email3.setPrimary(true);

            Email email4 = new Email();
            email4.setUser(user2);
            email4.setValue("any23@mailinatore.com");
            email4.setPrimary(false);

            user2.addEmail(email3);
            user2.addEmail(email4);

            final Role roleUser = this.roleRepository.findByRoleName("user");
            user2.setRole(roleUser);
            roleUser.addUser(user2);

            final User savedUser2 = this.userRepository.save(user2);
            assertThat(savedUser2).isNotNull();
            LOGGER.info(user2.toString());

            final Iterable<Email> emailIterable2 = this.emailRepository.save(savedUser2.getEmails());
            assertThat(savedUser2.getEmails()).hasSize(2);//It must have two emails.
            assertThat(savedUser2.getEmails()).containsAll(emailIterable2);

            retrieval();

        } catch (Exception e) {
            LOGGER.error("Exception caught {}", e.getMessage());
            Assert.fail();
        }


    }

    @Test
    public void testInsertSecondUserWithTwoEmails() {
        try {
            //Insertion
            final User user2 = new User();
            
            final UserDetails userDetails = new UserDetails();
            
            userDetails.
            setPassword("passwordPlainForNow3");
            userDetails.
            setSalt("saltPlainForNow3");
            userDetails.
            setUsername("ingasatu");
            userDetails.setFirstName(RandomStringUtils.randomAlphabetic(5));
            userDetails.setLastName(RandomStringUtils.randomAlphabetic(5));
            
            user2.setUserDetails(userDetails);

            Email email3 = new Email();
            email3.setUser(user2);
            email3.setValue("ikuuj@mailinatore.com");
            email3.setPrimary(true);

            Email email4 = new Email();
            email4.setUser(user2);
            email4.setValue("subarus@mailinatore.com");
            email4.setPrimary(false);

            user2.addEmail(email3);
            user2.addEmail(email4);

            Role roleUser = this.roleRepository.findByRoleName("user");
            roleUser.addUser(user2);


            user2.setRole(roleUser);

            final User savedUser2 = this.userRepository.save(user2);

            assertThat(savedUser2).isNotNull();
            LOGGER.info(user2.toString());

            final Iterable<Email> emailIterable2 = this.emailRepository.save(savedUser2.getEmails());
            assertThat(savedUser2.getEmails()).containsAll(emailIterable2);


            retrieval();

        } catch (Exception e) {
            LOGGER.error("Exception caught {}", e.getMessage());
            Assert.fail();
        }
    }

    @Test
    public void testInsertUserWithoutRole() {
        try {
            //Insertion
            final User user2 = new User();
            final UserDetails userDetails = new UserDetails();
            userDetails.setPassword(RandomStringUtils.randomAscii(5));
            userDetails.setSalt(RandomStringUtils.randomAscii(5));
            userDetails.setUsername("pedrito");
            userDetails.setFirstName(RandomStringUtils.randomAlphabetic(5));
            userDetails.setLastName(RandomStringUtils.randomAlphabetic(5));
            
            
            user2.setUserDetails(userDetails);

            Email email3 = new Email();
            email3.setUser(user2);
            email3.setValue(RandomStringUtils.randomAscii(5) + "@" + RandomStringUtils.randomAscii(5));
            email3.setPrimary(true);

            Email email4 = new Email();
            email4.setUser(user2);
            email4.setValue(RandomStringUtils.randomAscii(5) + "@" + RandomStringUtils.randomAscii(5));
            email4.setPrimary(false);

            user2.addEmail(email3);
            user2.addEmail(email4);

            final User savedUser2 = this.userRepository.save(user2);

            assertThat(savedUser2).isNotNull();
            LOGGER.info(user2.toString());

            final Iterable<Email> emailIterable2 = this.emailRepository.save(savedUser2.getEmails());
            assertThat(savedUser2.getEmails()).containsAll(emailIterable2);

            retrieval();

        } catch (Exception e) {
            LOGGER.error("Exception caught {}", e.getMessage());
            Assert.fail();
        }


    }

    private void retrieval() {
        try {
            Specification<User> spec = new UserUsernameSpecification("%a%");
            List<User> result = this.userRepository.findAll(spec);
        	
            //List<User> result = this.userRepository.findByUsername("%a%");
            LOGGER.info("Users {}", result);
            assertThat(result).isNotEmpty();


            for(final User user : result){

                List<Email> emails = this.emailRepository.findByUserID(user.getUserID());
                LOGGER.info("Emails {}", emails);

            }

        } catch (Exception e) {
            LOGGER.error("Exception caught {}", e.getMessage());
            Assert.fail();
        }
    }
}