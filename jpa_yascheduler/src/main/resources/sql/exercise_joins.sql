/*select * from roles,employee2,role_employee where role_employee.roleID = roles.roleID and role_employee.employeeID = employee2.employeeID;*/



select roles.roleID,roles.RoleName,employee2.employeeID,employee2.EmployeeName,role_employee.roleID,role_employee.employeeID
from roles
inner join role_employee on role_employee.roleid = roles.roleID
inner join employee2 on role_employee.employeeid = employee2.employeeID

union all

/*Chuchis does not have any role.*/
select roles.roleID,roles.RoleName,employee2.employeeID,employee2.EmployeeName,role_employee.roleID,role_employee.employeeID 
from roles 
right join role_employee on role_employee.roleID = roles.roleID
right join employee2 on role_employee.employeeID = employee2.employeeID
where role_employee.roleID is null

union all

select roles.roleID,roles.RoleName,employee2.employeeID,employee2.EmployeeName,role_employee.roleID,role_employee.employeeID from role_employee 
right join roles on role_employee.roleID = roles.roleID
left join employee2 on role_employee.employeeID = employee2.employeeID
where role_employee.roleID is null;



CREATE TABLE roles
(
 roleID INT,
 RoleName VARCHAR(20)
);

CREATE TABLE employee2
(
 employeeID INT,
 EmployeeName VARCHAR(20)
);

create table role_employee
(
   roleID INT,
   employeeID INT
);

INSERT INTO roles VALUES(1, 'Admin');
INSERT INTO roles VALUES(2, 'User');
INSERT INTO roles VALUES(3, 'Scheduler');
INSERT INTO roles VALUES(4, 'Service');
INSERT INTO roles VALUES(5, 'Keeper');
INSERT INTO roles VALUES(6, 'Cleaner');
INSERT INTO roles VALUES(7, 'Advisor');

INSERT INTO employee2 VALUES(1,'Rafferty');
INSERT INTO employee2 VALUES(2, 'Jones');
INSERT INTO employee2 VALUES(3, 'Heisenberg');
INSERT INTO employee2 VALUES(4, 'Robinson');
INSERT INTO employee2 VALUES(5, 'Smith');
INSERT INTO employee2 VALUES(6, 'Williams');
INSERT INTO employee2 VALUES(7, 'Chuchis');

insert into role_employee values (1,1);
insert into role_employee values (2,2);
insert into role_employee values (2,3);
insert into role_employee values (3,4);
insert into role_employee values (4,4);
insert into role_employee values (5,5);
insert into role_employee values (6,6);



