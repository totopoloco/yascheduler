package at.mavila.scheduler.jpa.entities;

import at.mavila.scheduler.jpa.util.LoggerEntityListener;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Created by mavila on 27.08.16.
 */
@Entity
@Table(name = "ROLES")
@AutoProperty
@EntityListeners(value = {LoggerEntityListener.class})
public class Role implements Serializable {
    /**
	 * serialVersionUID.
	 */
	private static final long serialVersionUID = -4047907872240969373L;

	@Id
    @SequenceGenerator(name = "roles_id_seq", sequenceName = "roles_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "roles_id_seq")
    private Long roleID;

    @Column(name = "name", nullable = false, length = 20, unique = true)
    private String name;
    @Column(name = "description", nullable = true, length = 255, unique = false)
    private String description;
    
    
    /*Joins*/
    @ManyToOne
    @Property(policy = PojomaticPolicy.NONE)
    private Role parentRole;
    
    @OneToMany(mappedBy = "parentRole", fetch = FetchType.EAGER)
    @Property(policy = PojomaticPolicy.NONE)
    private List<Role> childrenRoles = new ArrayList<>();
    
    @OneToMany(mappedBy = "role", fetch = FetchType.EAGER)
    @Property(policy = PojomaticPolicy.NONE)
    private Set<User> users = new HashSet<>();
    
    
    

    @Property(policy= PojomaticPolicy.NONE)
    @ManyToMany(mappedBy = "roles")
    private List<Action> actions = new ArrayList<>();

    public List<Action> getActions() {
        return actions;
    }

    public void setActions(List<Action> actions) {
        this.actions = actions;
    }

    public void addAction(final Action action){
        this.actions.add(action);
    }

    public Role() {
        this.name = "";
        this.description = "";
    }

    public Role(final String name1, final String description1) {
        this.name = name1;
        this.description = description1;
    }

    public Long getRoleID() {
        return this.roleID;
    }

    public void setRoleID(Long roleID1) {
        this.roleID = roleID1;
    }

    public String getName() {
        return this.name;
    }

    public String getDescription() {
        return this.description;
    }

    public Set<User> getUsers() {
        return this.users;
    }

    public void setUsers(final Set<User> users1) {
        this.users = users1;
    }

    public void addUser(final User user) {
        this.users.add(user);
    }





    public Role getParentRole() {
        return this.parentRole;
    }

    public void setParentRole(Role parentRole1) {
        this.parentRole = parentRole1;
    }

    public void addChildRole(Role childRole) {
        if (childRole == null) {
            return;
        }

        this.childrenRoles.add(childRole);
    }

    public List<Role> getChildrenRoles() {
        return this.childrenRoles;
    }

    public void setChildrenRoles(List<Role> childrenRoles1) {
        this.childrenRoles = childrenRoles1;
    }

	/**
	 * @param name1 the name to set
	 */
	public void setName(String name1) {
		this.name = name1;
	}

	/**
	 * @param description the description to set
	 */
	public void setDescription(final String description2) {
		this.description = description2;
	}


    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }
    
    
    
}
