package at.mavila.yascheduler.mapper;

/**
 * Created by mavila on 04.09.16.
 */
public interface Mapper<P,E> {
     E convertFromPojoToEntity(final P s);
     P convertFromEntityToPojo(final E t);
}
