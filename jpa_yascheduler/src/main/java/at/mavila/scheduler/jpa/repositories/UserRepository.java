package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

/**
 * Created by mavila on 27.08.16.
 */
public interface UserRepository extends JpaRepository<User, Long>, JpaSpecificationExecutor<User> {

    @Query(" from User u where u.userDetails.username like :username order by u.userDetails.username")
    List<User> findByUsername(@Param("username") final String username);

}
