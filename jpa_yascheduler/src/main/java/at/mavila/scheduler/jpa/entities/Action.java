package at.mavila.scheduler.jpa.entities;

import at.mavila.scheduler.jpa.util.LoggerEntityListener;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

/**
 * Entity implementation class for Entity: Action
 *
 */
@Entity
@Table(name = "ACTIONS")
@AutoProperty
@EntityListeners(value = {LoggerEntityListener.class})
public class Action implements Serializable {

	@Id
	@SequenceGenerator(name = "actions_id_seq", sequenceName = "actions_id_seq", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "actions_id_seq")
	private Long actionID;

	@Column(name = "name", nullable = false, length = 255, unique = true)
	private String name;
	private static final long serialVersionUID = 1L;
	
	@Property(policy= PojomaticPolicy.NONE)
	@ManyToMany(cascade = CascadeType.ALL)
	@JoinTable(name = "ACTION_ROLE",
			joinColumns = { @JoinColumn(name = "actionID") },
			inverseJoinColumns = { @JoinColumn(name = "roleID") })
	private List<Role> roles = new ArrayList<>();

	public List<Role> getRoles() {
		return roles;
	}

	public void setRoles(List<Role> roles) {
		this.roles = roles;
	}

	public void addRole(final Role role){
		this.roles.add(role);
	}

	public Long getActionID() {
		return this.actionID;
	}

	public void setActionID(Long actionID1) {
		this.actionID = actionID1;
	}

	public String getName() {
		return this.name;
	}

	public void setName(String name1) {
		this.name = name1;
	}

	@Override
	public boolean equals(Object o) {
		return Pojomatic.equals(this, o);
	}

	@Override
	public int hashCode() {
		return Pojomatic.hashCode(this);
	}

	@Override
	public String toString() {
		return Pojomatic.toString(this);
	}
}
