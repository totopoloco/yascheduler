package at.mavila.scheduler.jpa.repositories;

import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.testng.annotations.Test;

/**
 * Created by mavila on 27.08.16.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring-config-mysql.xml"})
@EnableJpaRepositories
@EnableTransactionManagement
@Test(enabled=false)
public class EmailRepositoryMySQLTest extends  EmailRepositoryInMemoryTest{
    @Override
    @Test(enabled = false)
    public void testFindByUsername() throws Exception {
        super.testFindByUsername();
    }
}
