package at.mavila.scheduler.jpa.repositories;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.transaction.annotation.EnableTransactionManagement;
import org.springframework.transaction.annotation.Transactional;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

/**
 * Created by mavila on 27.08.16.
 */
//@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring-config-mysql.xml"})
@EnableJpaRepositories
@EnableTransactionManagement
@Transactional
public class UserRepositoryMySQLTest extends UserRepositoryInMemoryTest {

    private static final Logger LOGGER = LoggerFactory.getLogger(UserRepositoryMySQLTest.class);

    @BeforeClass
    @Override
    public void setUp() throws Exception {
        super.setUp();
        LOGGER.info("MySQL");
    }

    @Override
    @Test(enabled = false)
    public void testInsertUserAdminWithTwoEmails() {
        super.testInsertUserAdminWithTwoEmails();
    }

    @Override
    @Test(enabled = false)
    public void testInsertUserWithTwoEmails() {
        super.testInsertUserWithTwoEmails();
    }

    @Override
    @Test(enabled = false)
    public void testInsertSecondUserWithTwoEmails() {
        super.testInsertSecondUserWithTwoEmails();
    }

    @Override
    @Test(enabled = false)
    public void testInsertUserWithoutRole() {
        super.testInsertUserWithoutRole();
    }
}
