package at.mavila.scheduler.jpa.entities;

import at.mavila.scheduler.jpa.util.LoggerEntityListener;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.*;
import java.io.Serializable;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

/**
 * Simple User class
 * Created by mavila on 27.08.16.
 */
@Entity
@Table(name = "USERS")
@AutoProperty
@EntityListeners(value = {LoggerEntityListener.class})
public class User implements Serializable {

    /**
     * serialVersionUID
     */
    private static final long serialVersionUID = -326377727866020111L;

    @Id
    @SequenceGenerator(name = "users_id_seq", sequenceName = "users_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "users_id_seq")
    private Long userID;

    @Embedded
    private UserDetails userDetails;




    @Version
    private Long version;

    /*
    Joins
    */
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    @Property(policy = PojomaticPolicy.NONE)
    private Set<Email> emails = new HashSet<>();


    @ManyToOne(fetch = FetchType.EAGER)
    @JoinColumn(name = "roleID")
    @Property(policy = PojomaticPolicy.NONE)
    private Role role;


    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    @Property(policy = PojomaticPolicy.NONE)
    private Set<Calendar> calendar = new HashSet<>();


    @Property(policy= PojomaticPolicy.NONE)
    @ManyToMany(mappedBy = "subscribers")
    private List<Calendar> subscribedCalendar = new ArrayList<>();


    /*
    Getters and setters
    */
    public Long getUserID() {
        return this.userID;
    }

    public void setUserID(final Long userID1) {
        this.userID = userID1;
    }


    public void addEmail(final Email email) {
        this.emails.add(email);
        if (email.getUser() != this) {
            email.setUser(this);
        }
    }

    public void addCalendar(final Calendar calendar) {
        this.calendar.add(calendar);
        if (calendar.getOwner() != this) {
            calendar.setOwner(this);
        }
    }

    public Role getRole() {
        return this.role;
    }

    public void setRole(Role role1) {
        this.role = role1;
    }


    public Set<Email> getEmails() {
        return this.emails;
    }

    public void setEmails(final Set<Email> emails1) {
        this.emails = emails1;
    }


    public Set<Calendar> getCalendar() {
        return calendar;
    }

    public void setCalendar(Set<Calendar> calendar) {
        this.calendar = calendar;
    }


    public UserDetails getUserDetails() {
        return this.userDetails;
    }

    public void setUserDetails(final UserDetails userDetails1) {
        this.userDetails = userDetails1;
    }


    public Long getVersion() {
        return version;
    }

    public void setVersion(Long version) {
        this.version = version;
    }

	
	/*
	 Equals, hashCode and toString 
	 */

    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }
}
