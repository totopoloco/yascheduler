package at.mavila.yascheduler.pojo;

import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;

import java.io.Serializable;

/**
 * Created by mavila on 04.09.16.
 */
@AutoProperty
public final class User implements Serializable{

    private final Long userID;

    public User(final Long userID){
        this.userID = userID;
    }

    public Long getUserID() {
        return userID;
    }

    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }

}
