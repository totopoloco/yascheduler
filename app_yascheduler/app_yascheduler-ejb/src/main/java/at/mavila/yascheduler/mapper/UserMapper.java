package at.mavila.yascheduler.mapper;

import at.mavila.yascheduler.pojo.User;

/**
 * Created by mavila on 04.09.16.
 */
public class UserMapper implements Mapper<User, at.mavila.scheduler.jpa.entities.User> {


    private static UserMapper INSTANCE = null;

    private UserMapper() {

    }


    private synchronized static void createInstance() {
        INSTANCE = new UserMapper();
    }

    public static UserMapper getInstance() {
        if (INSTANCE == null) {
            createInstance();
        }
        return INSTANCE;
    }


    @Override
    public at.mavila.scheduler.jpa.entities.User convertFromPojoToEntity(User s) {
        at.mavila.scheduler.jpa.entities.User user = new at.mavila.scheduler.jpa.entities.User();
        user.setUserID(s.getUserID());
        return user;
    }

    @Override
    public User convertFromEntityToPojo(at.mavila.scheduler.jpa.entities.User t) {
        return null;
    }
}
