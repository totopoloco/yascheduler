package at.mavila.scheduler.jpa.repositories;

import at.mavila.scheduler.jpa.entities.Action;
import org.springframework.data.jpa.repository.JpaRepository;

/**
 * Created by mavila on 01.09.16.
 */
public interface ActionRepository extends JpaRepository<Action, Long> {
}
