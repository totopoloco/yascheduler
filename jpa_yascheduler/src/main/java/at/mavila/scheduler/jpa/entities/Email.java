package at.mavila.scheduler.jpa.entities;

import at.mavila.scheduler.jpa.util.LoggerEntityListener;
import org.pojomatic.Pojomatic;
import org.pojomatic.annotations.AutoProperty;
import org.pojomatic.annotations.PojomaticPolicy;
import org.pojomatic.annotations.Property;

import javax.persistence.*;
import java.io.Serializable;

/**
 * Email information from the user.
 * The user can have several email addresses, but only one email address per user.
 * Created by mavila on 27.08.16.
 */
@Entity
@Table(name = "EMAILS")
@AutoProperty
@EntityListeners(value = {LoggerEntityListener.class})
public class Email implements Serializable {

    /**
     * serialVersionUID.
     */
    private static final long serialVersionUID = -8757437534L;


    @Id
    @SequenceGenerator(name = "emails_id_seq", sequenceName = "emails_id_seq", allocationSize = 1)
    @GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "emails_id_seq")
    private Long emailID;


    @Column(name = "value", nullable = false, length = 255, unique = true)
    private String value;

    @Column(name = "isPrimary", nullable = false)
    private boolean isPrimary;

    //Joins
    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userID", nullable = false)
    @Property(policy = PojomaticPolicy.NONE)
    private User user;

    public boolean isPrimary() {
        return this.isPrimary;
    }

    public void setPrimary(final boolean primary) {
        this.isPrimary = primary;
    }


    public String getValue() {
        return this.value;
    }

    public void setValue(final String email) {
        this.value = email;
    }

    public Long getEmailID() {
        return emailID;

    }

    public void setEmailID(Long emailID) {
        this.emailID = emailID;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }


    @Override
    public boolean equals(Object o) {
        return Pojomatic.equals(this, o);
    }

    @Override
    public int hashCode() {
        return Pojomatic.hashCode(this);
    }

    @Override
    public String toString() {
        return Pojomatic.toString(this);
    }

}
