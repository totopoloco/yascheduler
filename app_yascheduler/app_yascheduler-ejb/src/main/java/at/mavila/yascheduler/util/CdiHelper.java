package at.mavila.yascheduler.util;

/**
 * Created by 200000313 on 09.09.2016.
 */
public final class CdiHelper {


    private CdiHelper() {}

    public static <T> T getLocalBean(Class<T> beanType)
    {
        return javax.enterprise.inject.spi.CDI.current().<T>select(beanType).get();
    }
}
